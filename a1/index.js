let posts = []; 
let count = 1; 

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	count++
	showPosts(posts)
	alert('Post Successfully Added')
	clearFields()
});

document.querySelector('#form-edit-post').addEventListener('submit', (e)  => {
	e.preventDefault();
	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			
			showPosts(posts)
			alert("Post successfully updated")
			break
		}else{
			console.log("error");
		}
	}
})

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button class="btn btn-warning btn-sm" onClick="editPost('${post.id}')">Edit</button>
				<button class="btn btn-danger btn-sm" onClick="deletePost('${post.id}')">Delete</button>
			</div> 
			`;
	})
	document.querySelector('#post-entries').innerHTML = postEntries;
}




const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML
	
	document.querySelector(`#txt-edit-id`).value = id
	document.querySelector(`#txt-edit-title`).value = title
	document.querySelector(`#txt-edit-body`).value = body
}


const deletePost = (postId) => {
	let index = posts.findIndex(post => post.id === parseInt(postId));
	if(index > -1){
		posts.splice(index, 1)
		alert(`Successfully Deleted Post`);
	}
	showPosts(posts)
};



const clearFields = () => {
	document.querySelector('#txt-title').value = " ";
	document.querySelector('#txt-body').value = " ";
}